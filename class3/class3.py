# k = "namen"

# print(type(k))

# print(dir(k))

## single letter
#print(k.count('n'))


#print(k.encode())

# print(k.endswith('en'))


# k = "Hi\tI\tam\tgood\nbye"

# print(k.expandtabs(15))


# k = "kunaiam"

# print(k.find('am'))


# k = "your otp is {}. Don't share the otp with others"

# sms_data = k.format("12345")

# print(sms_data)

# otp = "12345"
# k = f"your otp is {otp}. Don't share the otp with others"
# print(k)

# k = "hello  {name}! your otp is {otp}. Don't share the otp with others"

# data = {"name": "kuna", "otp": "1234"}

# print(k.format_map(data))

# k = "happy"

##single char
#print(k.index("r"))

# k = "grsjhsdf"

# print(k.isalnum())

#k = input("Enter a string \n")

#print(k.isalnum())

# k = input("Enter a string \n")

#  print(k.isalpha())

k = "one_more_dir"

print(k.isidentifier())
