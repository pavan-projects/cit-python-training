## filter
# k = ["kavitha", "kuna", "rao", "ramesh", "suresh"]


# def name_start_with_k(name):
#     if name.startswith("k"):
#         return True
#     else:
#         return False
#
# data = filter(name_start_with_k, k)
# for each_ele in data:
#     print(each_ele)


# data = filter(lambda name: True if name.startswith("k") else False, k)
# for each_ele in data:
#     print(each_ele)

# import functools
#
# k = [1,2,3,4]
#
# def sum_fun(a,b):
#     return a+b
#
# data = functools.reduce(sum_fun, k)
# print(data)
#
# # assignment: replace sum_fun with lambda

## decorator

# def dec_add_2(fn):
#     def return_fun(a,b):
#         data = fn(a,b)
#         data = data + 2 #wrapper code
#         return data
#     return return_fun
#
#
# @dec_add_2
# def sum_fun(a,b):
#     return a+b
#
# # def do_sum():
# #     data = sum_fun(1,2)
# #     return data
#
# print(sum_fun(1,2))


def wrapper_fuc(fn):
    def inner_fun():
        output_data = fn()
        final_data = "Mr." + output_data.capitalize()
        return final_data
    return inner_fun

@wrapper_fuc
def name():
    data = input("Enter your name\n")
    return data

print(name())



