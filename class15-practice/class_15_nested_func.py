k = [1,[2,[3,[4,[5,6]]]]]

# output: [1,2,3,4,5,6]
result = []
def flat_list(inp_list):
    for each_ele in inp_list:
        if not str(each_ele).isnumeric():
            flat_list(each_ele)
        else:
            result.append(each_ele)
    return

flat_list(k)
print(result)
