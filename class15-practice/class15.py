k = [['Harry', 37.21], ['Berry', 37.21], ['Tina', 37.2], ['Akriti', 41.0], ['Harsh', 39.0], ['kuna', 37.2]]


def second_lowest_grade(input_list):
    marks_without_duplicate = []
    marks = [mark[1] for mark in input_list]
    for each_ele in marks:
        if not each_ele in marks_without_duplicate:
            marks_without_duplicate.append(each_ele)
    marks_without_duplicate.sort(reverse=True)
    second_mark = marks_without_duplicate[-2]

    names = []
    for name, mark in input_list:
        if mark == second_mark:
            names.append(name)
    names.sort()
    for each_name in names:
        print(each_name)


second_lowest_grade(k)
