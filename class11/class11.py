# # scope

# k = "I am global variable"

# def sample_function():
#     k = "I am local variable"
#     print(k)

# def sample_function():
#     global k
#     k = "I am local variable"
#     print(k)

# sample_function()
# print(k)

# k = ["1", "2", "3", "4"]
#
# def sample_function(a):
#     a.append("extra")
#     return a
#
# print("before sample function", k)
# sample_function(k)
# print("after sample function", k)

# # catch the error

# # try:
# #     statements
# # except Exception:
# #     statement

# def str_concat(str1, str2):
#     data = None
#     try:
#         data = str1 + str2
#     except Exception as e:
#         print("concatination has been  failed, system error statement is", str(e))
#     return data
#
# output = str_concat("hi", 12)
# print(output)

# def str_concat(str1, str2):
#     data = None
#     try:
#         data = str1 + str2
#         print("i am done")
#     except Exception as e:
#         print("concatination has been  failed, system error statement is", str(e))
#     finally:
#         print("Execution has been completed")
#     return data
#
# output = str_concat("hi", "bye")
# print(output)

# def str_concat(str1, str2):
#     data = None
#     try:
#         data = str1 + str2
#         print("i am done")
#     except Exception as e:
#         print("concatination has been  failed, system error statement is", str(e))
#     else:
#         print("all statements have been executed successfully")
#     return data
#
# output = str_concat("hi", "bye")
# print(output)

## map

k = [1,2,"hi",4,5]

def print_fun(a):
    print(a)
    return a

# print_fun(k[0])
# print_fun(k[1])
# print_fun(k[2])

data = map(print_fun, k)
for each_ele in data:
    pass




