# for  each_number in range(0, 15):
#     if each_number == 7:
#         continue
#     print(each_number)

# h = 0
#
#
# while h < 10:
#     h = h + 1
#     print(h)


k = ["kuna", "hi", 1, [1,2,3]]

# print(k)
# print("Datatype is", type(k))

## indexing
print(k[3][0])
print(type(k[3][0]))

## slicing
print(k[1:])


##list attributes

k = ["kuna", "hi", 1, [1,2,3]]

# print(k)
# print("Datatype is", type(k))

## indexing
print(k[3][0])
print(type(k[3][0]))

## slicing
print(k[1:])

## list attributes

# 'append', 'clear', 'copy', 'count',\
# 'extend', 'index', 'insert', 'pop',\
# 'remove',
# 'reverse', 'sort'


# k = ["kuna", "hi"]
#
# print(k)
#
# k.append("data")
#
# print(k)

#
# k = ["kuna", "kuna", "hi", "bye"]
#
# count_num = k.count("kuna")
# print(count_num)



# k = ["kuna", "hi"]
#
# a = k.copy()
#
# print(k)
#
# a[0] = "bye"
#
# print(a)
#
# print(k)



k = ["kuna", "kuna", "hi", "bye"]

h = [1,2,3,4]

# k.extend(h)

g = k + h

print(g)



k = ["kuna", "kuna", "hi", "bye"]

h = k.index("kuna")

print(h)


k = ["kuna", "hi"]

k.insert(1, "hey")

print(k)


k = ["kuna", "hi"]

last_ele = k.pop()

print(last_ele)


k = ["kuna", "hi"]


k.remove("kuna")

print(k)






