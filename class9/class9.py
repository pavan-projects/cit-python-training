#dict

# 'clear', 'copy', 'fromkeys', 'get', 'items',\
# 'keys', 'pop', 'popitem', 'setdefault', 'update', 'values'

## keys

# k = {"hi": "data", "bye": "another data"}
# print(k)
#
# print(k.keys())
# print(k.values())
#
# #output:
# dict_keys(['hi', 'bye'])
# dict_values(['data', 'another data'])

k = [1,2,3,4]

for each_item in k:
    print(each_item)

k = [(1,2),(3,4),(4,5,6,7,8),(5,6)]

for num_one,num_two,*a in k:
    print("first element", num_one)
    print("sec element", num_two)
    print(a)


k = {"hi": "start", "bye": "end"}

for key, value in k.items():
    print(key, value)


h = k.pop("bye")

print(h)
print(k)


k = {"hi": "start", "bye": "end"}

d = k.get("hi", "no such key")

print(d)


k = {}

z = k.fromkeys(h, "default value")

print(z)

student_marks = [{"name": "student1", "marks": {"che": 20, "m1": 40, "phy": 45}},
                 {"name": "student2", "marks": {"che": 40, "m1": 40, "phy": 45}}]

for each_stu_rec in student_marks:
    result = "Pass"
    for _, marks in each_stu_rec["marks"].items():
        if marks < 35:
            result = "Fail"
    print(each_stu_rec["name"], result)



