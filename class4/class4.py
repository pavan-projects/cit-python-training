k = [ "hi", " ", "i", " ",  "am", " ", "happy"]

g = ""

# print(dir(g))

h = g.join(k)


print("using g as empty string",h)


d = "".join(k)

print("without variable", d)


k = "AbCdE"

print(k.lower())


k = "hi i am good \n"
print(k.strip())
print("I am the next line print statement")


#mutable = "which can be changed"
#immutable = "which cannot be changed"


k = "kuna"

k =  "rao"

g  = "i am happy"

new_content = g.replace("happy", "sad")
print("this is the content replaced", new_content)
print("g is", g)


k = "i am happy"

g = k.split(" ")

print(g)


k = "192.160.1.1"

g = k.split(".")

print(g)

print(g[0])


## slicing

k = "kuna"



d = k[0:2]

print(d)

d = k[1:]

print(d)

d = k[-1]

print(d)


## casting


k = "1234"

#print(type(k))

g = int(k)

#print(g)
#print(type(g))


h = 1234

print(h, type(h))

g = str(h)

print(g, type(g))



## memory reference


k = ["hi","bye",3,4]

print("before replacing", k)

k[0] = "happy"


print("after replacing", k)

d = k


print(d)


d[0] = "unhappy"


print('*' * 40)

print(d)
print(k)




















