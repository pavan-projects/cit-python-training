# functions
# DRY( dont repeat yourself)


# def <fun name>(args):
#     statements
#     return value

# f(x) = y

def input_func():
    c = input("Enter c value\n")
    d = input("Enter d value\n")
    return c, d


def sum_func(var1, var2):
    return int(var1) + int(var2)


first_inp, sec_inp = input_func()
print(first_inp, sec_inp)

output_data = sum_func(first_inp, sec_inp)
print(output_data)






def input_func():
    # c = input("Enter c value\n")
    # d = input("Enter d value\n")
    c = 4
    d = 5
    return c, d


def sum_func(var1, var2):
    return int(var1) + int(var2)


first_inp, sec_inp = input_func()
print(first_inp, sec_inp)

# positional mapping
output_data = sum_func(var2=first_inp, var1=sec_inp)
print(output_data)


def input_func():
    # c = input("Enter c value\n")
    # d = input("Enter d value\n")
    c = 4
    d = 5
    return c, d


def sum_func(var1, var2, *extra_inp):
    print(extra_inp)
    return int(var1) + int(var2) + extra_inp[0] + extra_inp[1] + extra_inp[2]


first_inp, sec_inp = input_func()
print(first_inp, sec_inp)

# argument overloading
output_data = sum_func(first_inp, sec_inp, 10, 11, 22)
print(output_data)


def input_func():
    # c = input("Enter c value\n")
    # d = input("Enter d value\n")
    c = 4
    d = 5
    return c, d


def sum_func(var1, var2, **extra_inp):
    print(extra_inp)
    return int(var1) + int(var2)


first_inp, sec_inp = input_func()
print(first_inp, sec_inp)

# key argument overloading
output_data = sum_func(var1=first_inp, var2=sec_inp, inp1=10, inp2=11, inp3=22)
print(output_data)


def input_func():
    # c = input("Enter c value\n")
    # d = input("Enter d value\n")
    c = 4
    d = 5
    return c, d


def sum_func(var1, var2, **extra_inp):
    print(extra_inp)
    return int(var1) + int(var2)


first_inp, sec_inp = input_func()
print(first_inp, sec_inp)

# key word args should follow pos args
output_data = sum_func(first_inp, var2=sec_inp, inp1=10, inp2=11, inp3=22)
print(output_data)


# def sum_func(var1, var2, **extra_inp):
#     print(extra_inp)
#     return int(var1)+int(var2)


# lambda


sum_func = lambda var1,var2: int(var1)+int(var2)


output_data = sum_func(4,5)
print(output_data)


# list comprehension

h = [num for num in range(0,100)]
print(h)

