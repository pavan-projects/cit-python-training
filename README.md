# cit-python-training

# class 1
python install  
GDB compiler  
Interpreter and compiling


# class 3
Memory  
operators  
id/type/dir  
print  
datatypes int/float/str
String attributes 

# class 4
String attributes  

# class 5
String attribute  
for/else loop  
if loop  
nested  
break

# class 6
continue  
while loop  
list

# class7  
Practice  

# class8
SETS
tuple  
Dict 

# class9
Dict attributes  
Examples

# class10
functions
overloading the args
lambda

# class11
local and global variables  
catch the errors  
map

# class12
filter
reduce
decorator

# class13
import statements  
os operations  
path  
running python in cmd line  
file args (sys.argv)

# class14  
argparser
handling files

# class15
Hacker rank problems  
Nested list problem
Nested function problem

## TODO
built in functions  
buit in keywords
pypi 


# Tools  
Pycharm CE





